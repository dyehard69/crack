#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    FILE *fpOut;
    if (argc < 2)
    {
        printf("Error: Filename required.\n");
        exit(1);
    }
    else if (argc == 2)
    {
        fpOut = stdout;
    }
    else if (argc == 3)
    {
        fpOut = fopen(argv[2], "w");
    }
    
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        exit(2);
    }
    
    //fpOut = fopen(argv[2], "w");
    if (!fpOut)
    {
        fprintf(stderr, "Can't open %s for writing\n", argv[2]);
        exit(3);
    }
    
    char line[1000];
    
    while (fgets(line, 1000, fp) != NULL)
    {
        char *hashed = md5(line, strlen(line) - 1);
        fprintf(fpOut, "%s", hashed);
        //fprintf(stdout, "%s", buf);
        fprintf(fpOut, "\n");
        free(hashed);
    }
    
    fclose(fp);
    fclose(fpOut);
    
    /*
    //isalnum
    printf("Lines: %d\nCharacters: %d\nWords: %d\n", lineCount, charCount, wordCount);
    //printf("%s %d %s\n", inFile[0], argc, inFile[1]);
    */
}